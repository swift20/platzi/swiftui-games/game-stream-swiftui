//
//  ProfileView.swift
//  GameStream
//
//  Created by Jorge Teofanes Vicuña Valle on 20/12/21.
//

import SwiftUI

struct ProfileView: View {
    
    
    @State var nombreUsuario:String = "Nombre Persona"
    @State var imagenPerfil:UIImage = UIImage(named: "perfil")!
    
    
    var body: some View {
       
            
        ZStack {
            
            Color("Marine").ignoresSafeArea().navigationBarHidden(true)
                .navigationBarBackButtonHidden(true)
                .navigationBarTitle("",displayMode: .inline)
            
            ScrollView {
                VStack{
                        
                      
                        VStack{
                                
                              
                            Image(uiImage: imagenPerfil ).resizable().aspectRatio(contentMode: .fill)
                                .frame(width: 150.0, height: 150.0)
                                .clipShape(Circle())
                            Text(nombreUsuario)
                                .fontWeight(.bold)
                                .foregroundColor(Color.white).font(.system(size: 20))
                                

                        }.padding(EdgeInsets(top: 64, leading: 0, bottom: 32, trailing: 0))
                    
                   
                        Text("Ajustes")
                            .fontWeight(.bold)
                            .foregroundColor(Color.white).frame(minWidth: 0, idealWidth: 100, maxWidth: .infinity,  alignment: .leading).padding(.leading,18).font(.system(size: 20))
                    
               
                    ModuloAjustes()
                 
                    Spacer()
                }
            }
            
         
        } .onAppear(
            
            perform: {
                
              //validar cuando no hay foto guardada
               
                
                
                if returnUiImage(named: "fotoperfil") != nil {
                    
                    imagenPerfil = returnUiImage(named: "fotoperfil")!
    
                }else{
                    print("no encontre foto de perfil en base de datos")
                    
                }
                
                
                
                
                print("revisando si tengo datos en user defaults")
                
                if UserDefaults.standard.object(forKey: "datosUsuario") != nil {
                    
                    nombreUsuario = UserDefaults.standard.stringArray(forKey: "datosUsuario")![2]
                    print("Si encontre nombre de usuario \(nombreUsuario)")
                }else{
                    
                    print("no encontre nombre de usuario guardado en objeto global de userdefaults")
                    
                }
                
            }
        
        
        )
       
   }
    
    
    func returnUiImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
}


struct ModuloAjustes:View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode> //para ir a la vista anterior
    @State var isToggleOn = true
    @State var isEditProfileViewActive = false
    
    var body: some View{
        
        
        VStack{
                 
            Button(action: {}, label: {
                    HStack { Text("Cuenta")
                    .foregroundColor(Color.white)
                    .font(.system(size: 15))
                    Spacer()
                    Image(systemName: "chevron.right")}.padding()
            }) .background(Color("Blue-Gray"))
            .clipShape(RoundedRectangle(cornerRadius: 1.0)).padding(.horizontal, 8.0)
            
            Button(action: {}, label: {
                    HStack { Text("Notificaciones")
                    .foregroundColor(Color.white)
                    .font(.system(size: 15))

                    Spacer()
                    
                        Toggle("", isOn: $isToggleOn)
                    
                    }.padding()
            }) .background(Color("Blue-Gray"))
            .clipShape(RoundedRectangle(cornerRadius: 1.0)).padding(.horizontal, 8.0)
            
            Button(action: {isEditProfileViewActive = true}, label: {
                    HStack { Text("Editar Perfil")
                            .font(.system(size: 15))

                    .foregroundColor(Color.white)
                    Spacer()
                    Image(systemName: "chevron.right")}.padding()

            }) .background(Color("Blue-Gray"))
            .clipShape(RoundedRectangle(cornerRadius: 1.0)).padding(.horizontal, 8.0)
            
           
            Button(action: {}, label: {
                    HStack { Text("Califica esta aplicación")
                            .font(.system(size: 15))

                    .foregroundColor(Color.white)
                    Spacer()
                    Image(systemName: "chevron.right")}.padding()

            }) .background(Color("Blue-Gray"))
            .clipShape(RoundedRectangle(cornerRadius: 1.0)).padding(.horizontal, 8.0)
            
            Divider()
                .frame(height: 1)
            
            Button(action:{self.presentationMode.wrappedValue.dismiss()}) {
                Text("Salir")
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .font(.system(size: 20))
                    .frame( maxWidth: 200, alignment: .center)
                    .padding(EdgeInsets(top: 11, leading: 18, bottom: 11, trailing: 18))
                    .overlay(RoundedRectangle(cornerRadius: 6)
                                .stroke(Color("Dark-Cian"), lineWidth: 1).shadow(color: .white, radius: 6))
                
            }.padding(.bottom)
                
            NavigationLink(
                destination: EditProfileView()
                ,
                isActive: $isEditProfileViewActive,
                label: {
                    EmptyView()
                })
            
            
        }
        
        
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
