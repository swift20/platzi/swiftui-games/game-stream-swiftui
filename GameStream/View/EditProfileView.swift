//
//  EditProfileView.swift
//  GameStream
//
//  Created by Jorge Teofanes Vicuña Valle on 20/12/21.
//

import SwiftUI

struct EditProfileView: View {
    
    
   
    @State var imagenPerfil:UIImage = UIImage(named: "perfil")!
    @State var imagenPerfilBase: Image? = Image("perfil")
    @State var isCameraActive = false
    
    var body: some View {
        ZStack {
            
            Color("Marine").ignoresSafeArea()
            
            ScrollView{
                    
               
                
                    VStack(alignment: .center){
                       
                        
                        
                        
                        Button(action: {isCameraActive = true}, label: {
                            ZStack{
                                        
                                imagenPerfilBase!.resizable().aspectRatio(contentMode: .fill)
                                    .frame(width: 118.0, height: 118.0)
                                    .clipShape(Circle())                      .sheet(isPresented: $isCameraActive, content: {
                                        SUImagePickerView(sourceType: .photoLibrary , image: self.$imagenPerfilBase, isPresented: $isCameraActive)
                                    })

                                
                                Image(systemName: "camera").foregroundColor(.white)
           
               
                                
                            }
                        })
                        
                        Text("Elije una foto de perfíl")
                            .fontWeight(.bold)
                            .foregroundColor(Color.white)

                    }.padding(.bottom,18)
                    
                  ModuloEditar()
            }
        }.onAppear(perform: {
            //validar cuando no hay foto guardada
             
              if returnUiImage(named: "fotoperfil") != nil {

                  imagenPerfil = returnUiImage(named: "fotoperfil")!
                  imagenPerfilBase = Image(uiImage: imagenPerfil)

              }else{
                  print("no encontre foto de perfil en base de datos")

              }
        })
    }
    
    func returnUiImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
}


struct ModuloEditar:View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode> //para ir a la vista anterior

    @State var correo:String = ""
    @State var contraseña:String = ""
    @State var nombre:String = ""
   
    
    var body: some View{
        
        
        VStack(alignment: .leading){
            
            Text("Correo electrónico")
                .foregroundColor(Color(red: 63/255, green: 202/255, blue: 160/255, opacity: 1.0))
            
            ZStack(alignment: .leading){
                if correo.isEmpty { Text("ejemplo@gmail.com").font(.caption).foregroundColor(Color(red: 174/255, green: 177/255, blue: 185/255, opacity: 1.0)) }
                
                TextField("", text: $correo).foregroundColor(.white)
            }
            
            Divider()
                .frame(height: 1)
                .background(Color("Dark-Cian")).padding(.bottom)
            
            
            Text("Contraseña").foregroundColor(.white)
            
            
            ZStack(alignment: .leading){
                if contraseña.isEmpty { Text("Introduce tu nueva contraseña").font(.caption).foregroundColor(Color(red: 174/255, green: 177/255, blue: 185/255, opacity: 1.0)) }
                
                SecureField("", text: $contraseña).foregroundColor(.white)
                
            }
            
            Divider()
                .frame(height: 1)
                .background(Color("Dark-Cian")).padding(.bottom)
            
            Text("Nombre").foregroundColor(.white)
            
            
            ZStack(alignment: .leading){
                if nombre.isEmpty { Text("Introduce tu nombre de usuario").font(.caption).foregroundColor(Color(red: 174/255, green: 177/255, blue: 185/255, opacity: 1.0)) }
                
                TextField("", text: $nombre).foregroundColor(.white)
                
            }
            
            Divider()
                .frame(height: 1)
                .background(Color("Dark-Cian")).padding(.bottom,32)
            
            Button(action:{ actualizarDatos()}) {
                Text("Actualizar Datos")
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .frame( maxWidth: .infinity, alignment: .center)
                    .padding(EdgeInsets(top: 11, leading: 18, bottom: 11, trailing: 18))
                    .overlay(RoundedRectangle(cornerRadius: 6)
                                .stroke(Color("Dark-Cian"), lineWidth: 1).shadow(color: .white, radius: 6))
                
            }.padding(.bottom)

            

            
            
       
            
        }.padding(.horizontal, 42.0).onAppear(
            perform: {
        
              
              print("revisando si tengo datos en user defaults")
              
              if UserDefaults.standard.object(forKey: "datosUsuario") != nil {
                  
                  nombre = UserDefaults.standard.stringArray(forKey: "datosUsuario")![2]
                  correo = UserDefaults.standard.stringArray(forKey: "datosUsuario")![0]
                  contraseña = UserDefaults.standard.stringArray(forKey: "datosUsuario")![1]
                  print("Si encontre nombre de usuario \(nombre), \(correo)")
              }else{
                  
                  print("no encontre nombre de usuario guardado en objeto global de userdefaults")
                  
              }
              
          }
        )
        
        
    }
    
    func actualizarDatos()  {
        let objetoActualizadorDatos = SaveData()

        let resultado = objetoActualizadorDatos.guardarDatos(correo: correo, contrasena: contraseña, nombre: nombre)

        print("Se guardaron los datos con exito?: \(resultado)")
        
        self.presentationMode.wrappedValue.dismiss() //para ir a la vista anterior
    }
}


struct EditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        EditProfileView()
    }
}
