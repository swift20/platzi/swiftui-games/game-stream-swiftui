//
//  ContentView.swift
//  GameStream
//
//  Created by Jorge Teofanes Vicuña Valle on 17/12/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            ZStack{
                
                Spacer()
                
                Color("Marine").ignoresSafeArea()
                
                VStack{
                    Image("appLogo").resizable().aspectRatio( contentMode: .fit).frame(width:250)
                        .padding(.bottom, 42.0)
                    
                    InicioyRegistroView()
                }.padding(.top,20)
            }.navigationBarHidden(true)
        }.navigationViewStyle(StackNavigationViewStyle())//para que la navegacion en horizontal sea completa, y evito el codigo siguiente que esta comentado
//        .environment(\.horizontalSizeClass, .compact) //para que la navegacion en horizontal sea completa
    }
}


struct InicioyRegistroView: View{
    @State private var tipoInicioSesion = true
    var body: some View{
        HStack {
            Spacer()
            Button("Inicia Sesion") {
                tipoInicioSesion = true
                print("Pantalla inicio sesión")
            }.foregroundColor(tipoInicioSesion ? .white : .gray)
            Spacer()
            Button("Registrate") {
                
                tipoInicioSesion = false
                print("Pantalla inicio sesión")
            }.foregroundColor(tipoInicioSesion ? .gray : .white)
            Spacer()
        }
        Spacer(minLength: 42)
        if tipoInicioSesion == true{
            InicioSesionView()
        }else{
            RegistroView()
        }
        
    }
}

struct InicioSesionView:View{
    @State private var correo = ""
    @State private var contrasena = ""
    
    @State var isPantallaHomeActive = false
    @State var ifNotUserFound = false

    var body: some View{
        
        ScrollView {
            VStack(alignment: .leading) {
                
                
                Text("Correo Electrónico").foregroundColor(Color("Dark-Cian"))
                
                ZStack(alignment:.leading){
                    
                    if correo.isEmpty{
                        //verbatim coloque, porque sino el correo me salia como link
                        Text(verbatim: "ejemplo@gmail.com").font(.caption).foregroundColor(.gray)
                    }

                    TextField("", text: $correo).foregroundColor(.white)
                    

                    
                }
                Divider().frame(height:1).background(Color("Dark-Cian")).padding(.bottom)
                
                Text("Contraseña").foregroundColor(Color("Dark-Cian"))
                ZStack(alignment:.leading){
                    
                    if contrasena.isEmpty{
                        Text("Contraseña").font(.caption).foregroundColor(.gray)
                    }

                    SecureField("", text: $contrasena).foregroundColor(.white)
                    

                    
                }
                Divider().frame(height:1).background(Color("Dark-Cian")).padding(.bottom)
                
                Text("¿Olvidaste Contraseña?").font(.footnote)
                    .frame(width: 300, alignment: .trailing)
                    .foregroundColor(Color("Dark-Cian"))
                    .padding(.bottom)
                
                Button(action: iniciarSesion, label: {
                    Text("INICIAR SESION")
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .frame( maxWidth: .infinity, alignment: .center)
                        .padding(EdgeInsets(top: 11, leading: 18, bottom: 11, trailing: 18))
                        .overlay(RoundedRectangle(cornerRadius: 6.0).stroke(Color("Dark-Cian"), lineWidth: 1.0).shadow(color: .white ,radius: 6))
                    
                }).alert(isPresented: $ifNotUserFound) {
                    Alert(title: Text("Error"), message: Text("No se encontro el usuario o la contraseña es incorrecta"), dismissButton: .default(Text("Entendido")))
                }
                
                Text("Regístrate con redes sociales").frame( maxWidth: .infinity,  alignment: .center).foregroundColor(.white)
                
                
                HStack {
                        
    
                        Button(action: {print("Inicio de sesión con Facebook")}) {
                            Text("Facebook")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                                .padding(.vertical, 8.0)
                                .frame( maxWidth: .infinity, alignment: .center)
                                .background(Color("Blue-Gray"))
                                .clipShape(RoundedRectangle(cornerRadius: 4.0))
                            
                        }
                        
                        .padding()
                    
                    
                    
                    Button(action: {print("Inicio de sesión con Twitter");isPantallaHomeActive.toggle()}) {
                        Text("Twitter")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .padding(.vertical, 8.0)
                            .frame( maxWidth: .infinity ,alignment:
                                            .center)
                            .background(Color("Blue-Gray"))
                            .clipShape(RoundedRectangle(cornerRadius: 4.0))
                    }.padding()
                    
                    
                }
                
            }.padding(.horizontal, 77.0)
            
            NavigationLink(destination: HomeView(),
                           isActive: $isPantallaHomeActive,
                           label: {
                EmptyView()
                
            })
        }
    }
    func iniciarSesion() {
        
       let objetoDatosUsuario = SaveData()
        
         print("Mi correo es \(correo) y mi contraseña es \(contrasena)")
        
        if objetoDatosUsuario.validar(correo: correo, contrasena: contrasena){
            isPantallaHomeActive.toggle()
            ifNotUserFound = false
        }else{
            //Comentar linea de abajo para habilitar funcionalidad de validacion de usuario.
//            isPantallaHomeActive.toggle()
            ifNotUserFound = true
            print("Tus datos son incorrectos")
            
        }
        
        
            
    }
}





struct RegistroView:View{
    @State var correo:String = ""
    @State var contrasena:String = ""
    @State var confirmacionContraseña:String = ""

    var body: some View{
        ScrollView {
            
            
            
            VStack(alignment: .center){
                
                Text("Elije una foto de Perfil")
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                
                Text("Puedes cambiar o elegirla mas adelante").fontWeight(.light).foregroundColor(.gray).padding(.bottom)
                Button(action: tomarFoto, label: {
                    ZStack {
                        Image("perfilEjemplo").resizable().aspectRatio(contentMode: .fit)
                            .frame(width: 80, height: 80)
                        Image(systemName: "camera").foregroundColor(.white)
                    }
                }).padding(.bottom)
            }
            
            VStack {
                VStack (alignment: .leading){
                Text("Correo Electrónico*").foregroundColor(Color("Dark-Cian"))
                        .frame(width: 300,alignment: .leading)

                ZStack(alignment:.leading){

                    if correo.isEmpty{
                        //verbatim coloque, porque sino el correo me salia como link
                        Text(verbatim: "ejemplo@gmail.com").font(.caption).foregroundColor(.gray)
                    }

                    TextField("", text: $correo).foregroundColor(.white)



                }
                Divider().frame(height:1).background(Color("Dark-Cian")).padding(.bottom)

                Text("Contraseña*").foregroundColor(Color("Dark-Cian"))
                ZStack(alignment:.leading){

                    if contrasena.isEmpty{
                        Text("Contraseña").font(.caption).foregroundColor(.gray)
                    }

                    SecureField("", text: $contrasena).foregroundColor(.white)
                }
                Divider().frame(height:1).background(Color("Dark-Cian")).padding(.bottom)

                Text("Confirmar Contraseña*").foregroundColor(Color("Dark-Cian"))
                ZStack(alignment:.leading){

                    if confirmacionContraseña.isEmpty{
                        Text("Reintroduce tu Contraseña").font(.caption).foregroundColor(.gray)
                    }

                    SecureField("", text: $confirmacionContraseña).foregroundColor(.white)



                }
                
                Divider().frame(height:1).background(Color("Dark-Cian")).padding(.bottom)



            }
                
                Button(action: registrate, label: {
                    Text("REGISTRATE")
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .frame( maxWidth: .infinity, alignment: .center)
                        .padding(EdgeInsets(top: 11, leading: 18, bottom: 11, trailing: 18))
                        .overlay(RoundedRectangle(cornerRadius: 6.0).stroke(Color("Dark-Cian"), lineWidth: 1.0).shadow(color: .white ,radius: 6))
                }).padding(.top)

                Text("Regístrate con redes sociales").frame( maxWidth: .infinity,  alignment: .center).foregroundColor(.white)


                HStack {


                        Button(action: {print("Inicio de sesión con Facebook")}) {
                            Text("Facebook")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                                .padding(.vertical, 8.0)
                                .frame( maxWidth: .infinity, alignment: .center)
                                .background(Color("Blue-Gray"))
                                .clipShape(RoundedRectangle(cornerRadius: 4.0))

                        }

                        .padding()



                    Button(action: {print("Inicio de sesión con Twitter")}) {
                        Text("Twitter")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .padding(.vertical, 8.0)
                            .frame( maxWidth: .infinity ,alignment:
                                            .center)
                            .background(Color("Blue-Gray"))
                            .clipShape(RoundedRectangle(cornerRadius: 4.0))
                    }.padding()


                }

            }.padding(.horizontal, 77.0)
        }

    }
    func tomarFoto() {
        print("Voy  a tomar fotografia")
    }

    func registrate()  {
        
        print("Me registro con el correo \(correo), la contraseña \(contrasena) y confirmación de contraseña \(confirmacionContraseña)")

        //validación contraseña
        if contrasena == confirmacionContraseña{
          
            let objetoActualizadorDatos = SaveData()
            
            let resultado = objetoActualizadorDatos.guardarDatos(correo: correo, contrasena: contrasena, nombre: "")
            
            print("Se guardaron los datos con exito?: \(resultado)")
            
        }else{
            
            print("Contraseñas diferentes, vuelve a intentarlo")
        }
        
        
        
    }
}




struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
//.previewInterfaceOrientation(.portraitUpsideDown)
    }
}
