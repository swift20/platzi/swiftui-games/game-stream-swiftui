//
//  GameStreamApp.swift
//  GameStream
//
//  Created by Jorge Teofanes Vicuña Valle on 17/12/21.
//

import SwiftUI

@main
struct GameStreamApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
